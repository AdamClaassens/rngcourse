import {Button, FlatList, StyleSheet, View} from 'react-native';
import {useState} from "react";
import GoalItem from "./components/GoalItem";
import GoalInput from "./components/GoalInput";
import {StatusBar} from "react-native";

export default function App() {
    const [modalIsVisible, setModalIsVisible] = useState(false)
    const [courseGoals, setCourseGoals] = useState([])

    const startAddGoalHandler = () => {
        setModalIsVisible(true)
    }

    const endAddGoalHandler = () => {
        setModalIsVisible(false)
    }

    const addGoalHandler = (enteredGoalText) => {
        setCourseGoals((currentCourseGoals) => [
            ...currentCourseGoals,
            {
                id: Math.random().toString(),
                text: enteredGoalText
            }
        ])

        endAddGoalHandler()
    }

    const deleteGoalHandler = (id) => {
        setCourseGoals((currentCourseGoals) => {
            return currentCourseGoals.filter((goal) => goal.id !== id)
        })
    }

    return (
        <>
            {/* Status bad allows the op of the screen that has battery, time etc to still be visible */}
            <StatusBar />
            <View style={styles.appContainer}>
                <Button
                    title={"Add New Goal"}
                    color={'#b180f0'}
                    onPress={startAddGoalHandler}
                />
                <GoalInput showModal={modalIsVisible} onAddGoal={addGoalHandler} onCancel={endAddGoalHandler}/>
                <View style={styles.goalsContainer}>

                    {/* Here we are using FlatList instead of ScrollView since it will not render all items on load*/}
                    {/* Keep in mind for FlatList we changed the data from a variable to an object that has key and value pairs*/}
                    <FlatList
                        data={courseGoals}
                        // This provides the key to stop duplicate keys in a list
                        // If they key was called key in you object, keyExtractor would not be needed
                        keyExtractor={(item, index) => {
                            return item.id
                        }}
                        renderItem={
                            itemData => {
                                return <GoalItem
                                    id={itemData.item.id}
                                    text={itemData.item.text}
                                    onDeleteItem={deleteGoalHandler}
                                />
                            }
                        }
                    />

                    {/* Manually build list of items. ScrollView will load all items on load*/}
                    {/*<ScrollView>*/}
                    {/*    {courseGoals.map((goal) =>*/}
                    {/*        <View*/}
                    {/*            key={goal}*/}
                    {/*            style={styles.goalItem}*/}
                    {/*        >*/}
                    {/*            <Text*/}
                    {/*                style={styles.goalText}*/}
                    {/*            >*/}
                    {/*                {goal}*/}
                    {/*            </Text>*/}
                    {/*        </View>*/}
                    {/*    )}*/}
                    {/*</ScrollView>*/}
                </View>
            </View>
        </>
    );
}

const styles = StyleSheet.create({
    appContainer: {
        flex: 1,
        paddingTop: 50,
        paddingHorizontal: 16,
    },
    inputContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 24,
        borderBottomWidth: 1,
        borderColor: '#cccccc'
    },
    textInput: {
        borderWidth: 1,
        borderColor: '#cccccc',
        width: '70%',
        marginRight: 8,
        padding: 8
    },
    goalsContainer: {
        flex: 5,
    }
});
