import {Text, View, StyleSheet, Pressable} from "react-native";

const GoalItem = (props) => {
    return (
        <View
            style={styles.goalItem}
        >

            <Pressable
                // This is a style that effects a pressed item on Android
                android_ripple={{color: '#210644'}}
                onPress={props.onDeleteItem.bind(this, props.id)}
                // This is a style that effects a pressed item on IOS
                style={({pressed}) => pressed && styles.pressedItem}
            >
                <Text
                    style={styles.goalText}
                >
                    {props.text}
                </Text>

            </Pressable>
        </View>
    )
}

const styles = StyleSheet.create({
    goalItem: {
        margin: 8,
        borderRadius: 6,
        backgroundColor: '#5e08cc',
    },
    goalText: {
        padding: 8,
        color: 'white'
    },
    pressedItem: {
        opacity: 0.5
    }
})

export default GoalItem